import json

def Scrub
f = open('test.json','r')
JSON_OBJECT = json.load(f)
f.close()
# print(JSON_OBJECT['getRecipeDetailsResponse']['recipes'][0]['recipeIngredients']) # to debug pre pop
for i in JSON_OBJECT['getRecipeDetailsResponse']['recipes'][0]['recipeIngredients']:
    i.pop('promotions',None)
# print(JSON_OBJECT['getRecipeDetailsResponse']['recipes'][0]['recipeIngredients']) # to debug post pop
