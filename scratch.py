from requests import Session

session = Session()

# HEAD requests ask for *just* the headers, which is all you need to grab the
# session cookie
req = requests.get('https://www.allrecipes.com/recipe/7000/golden-crescent-rolls/')

response = requests.post(
    url='https://allrecipes.groceryserver.com/groceryserver/service/recipe/rest/v10/clientId/bfeb1eb4e751f03bceffaa649e977927/getRecipeInformationByExternalId',
    data={
        "request":{"externalIds":["7000"]}
    },
    headers={
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0",
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.5",
        "Content-type": "text/plain"
    },
    referrer=
)

response.get

response.re
session.
print(response.text)

#{"request":{"recipeSourceId":224,,"externalRecipeUrl":"https://www.allrecipes.com/recipe/7000/golden-crescent-rolls/"}}