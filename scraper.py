import http.client
import urllib.request
# from pymongo import MongoClient
import pprint
from recipe_scrapers import scrape_me

def get_recipe(url):
    scraper = scrape_me(url)
    return scraper

# ##Global variables
url = "https://www.allrecipes.com/recipe/" 

def check_recipe(url):
    try:
        urllib.request.urlopen(url)
        return True
    except (urllib.error.HTTPError):
        print(url,'whoops')

# def add_recipe(scraper):
#     recipe = scraper
#     client = MongoClient('10.0.75.1',8081)
#     db = client.recipes
#     collection = db.test
#     post =  {
#             "name": recipe.title,
#             "total_time": recipe.total_time,
#             "servings": recipe.yields,
#             "ingredients": recipe.ingredients,
#             "instructions": recipe.instructions
#             }
#     post_id = collection.insert_one(post).inserted_id
#     post_id

i=0
good_URLs = []
while i<=15000:#999999:
    urlToCheck = url + str(i).zfill(10)
    if check_recipe(urlToCheck) == True:
        good_URLs.append(urlToCheck)
        scraper = get_recipe(urlToCheck)
        print (scraper.title)
        print (scraper.total_time)
        print (scraper.yields)
        print (scraper.ingredients)
        # add_recipe(scraper)
    i+=1000
print(good_URLs)