import http.client
import urllib.request
# from pymongo import MongoClient
import pprint
import json
from io import BytesIO
import os
import pycurl
from requests import Request, Session


def check_recipe(url):
    try:
        urllib.request.urlopen(url)
        return True
    except (urllib.error.HTTPError):
        print(url,'whoops')

# # def add_recipe(recipe)
# #     client = MongoClient('10.0.75.1',8081)
# #     db = client.recipes
# #     collection = db.test
# #     post = {'test': 'test'}
# #     post_id = collection.insert_one(post).inserted_id
# #     post_id

i=0
good_URLs = []
while i<=15000:#999999:
    iPadded= str(i).zfill(10)
    urlToCheck = 'https://www.allrecipes.com/recipe/{}/'.format(iPadded)
    if check_recipe(urlToCheck) == True:
        buffer = BytesIO()
        url ='https://allrecipes.groceryserver.com/groceryserver/service/recipe/rest/v10/clientId/bfeb1eb4e751f03bceffaa649e977927/getRecipeInformationByExternalId'
        body = {
            'request':{
                'externalIds':[iPadded],
                'maxResult':'100',
                'zipCode':'92010'
                }
            }
        pf = urllib.parse.urlencode(body)
        headers = {
            'Accept' : '*/*',
            'Accept-Encoding':'gzip, deflate, br',
            'Accept-Language':'en-US,en;q=0.5',
            'Cache-Control':'no-cache',
            'Connection':'keep-alive',
            'Content-type':'application/json;charset=UTF-8',
            'DNT':'1',
            'Host':'allrecipes.groceryserver.com',
            'Origin':'https://www.allrecipes.com/',
            'Pragma':'no-cache',
            'Referer':urlToCheck,
            'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0'
        }
        header = [
            'Accept : */*',
            'Accept-Encoding: gzip, deflate, br',
            'Accept-Language: en-US,en;q=0.5',
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            'DNT: 1',
            'Host: allrecipes.groceryserver.com',
            'Origin: https://www.allrecipes.com/',
            'Pragma: no-cache',
            f'Referer: {urlToCheck}',
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0'
        ]
        # data = urllib.parse.urlencode(body).encode()
        # req = urllib.request.Request(url, headers=headers,data=bytes(json.dumps(body), encoding="utf-8"))
        # print(data)
        # print(req)
        # try: 
        #     res = urllib.request.urlopen(req)
        #     print(res.read())
        #     JSON_object = json.loads(res.read())
        # except urllib.error.URLError as e:
        #     print(e.reason) 
        crl = pycurl.Curl()
        crl.setopt(crl.URL, url)
        crl.setopt(crl.SSL_VERIFYPEER, 0)   
        crl.setopt(crl.SSL_VERIFYHOST, 0)
        crl.setopt(crl.VERBOSE, True)
        crl.setopt(crl.POSTFIELDS, pf)
        crl.setopt(crl.WRITEDATA, buffer)
        crl.setopt(crl.FOLLOWLOCATION, True)
        crl.setopt(crl.HTTPHEADER,header)
        crl.perform()
        crl.close()
        response = buffer.getvalue().decode('utf-8')
        print(response)
    i+=1000
print(good_URLs)


# What do i knoow for sure?

# Well for one thing the the site returns a different layout depending on the URL.
# Preceding zeros force the application to load in a way that generates a XHR to groceryserver
# forcing it to return a JSON doc presumably for lookup to groceryserver which is afk.

# The raw header of the request contains 
# # # # Host: allrecipes.groceryserver.com
# # # # User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0
# # # # Accept: */*
# # # # Accept-Language: en-US,en;q=0.5
# # # # Accept-Encoding: gzip, deflate, br
# # # # Content-type: text/plain
# # # # Content-Length: 434
# # # # Origin: https://www.allrecipes.com
# # # # Connection: keep-alive
# # # # Referer: https://www.allrecipes.com/recipe/000259768/ 
# # # # DNT: 1

# The body of the request contains
# body = {
#         'externalIds':str(i).zfill(10),
#         'maxResult':'100',
#         'retailerLocationIds':[60467,84117,78314,70218,60466,113004,60465,90373,60343,60344,60532,60462,60461,60458,60468,86023,60535,60453,78317,111729,60547,78913,60402,113600], ##not necessary
#         ##'recipeSourceId':224, ##not necessary
#         ##'latitude':'33.16389',##not necessary
#         ##'longitude':'-117.30030',##not necessary
#         'zipCode':'92010', ## necessary
#         ##'version':'20200225', ##not necessary
#     }
# ##not necessary

# headers= {
#     'Accept' : '*/*',
#     'Accept-Encoding':'gzip, deflate, br'
#     'Accept-Language':'en-US,en;q=0.5'.
#     'Cache-Control','value':'no-cache'.
#     'Connection':'keep-alive'.
#     'Content-Length':'434'.
#     'Content-type':'text/plain'.
#     'DNT':'1'.
#     'Host':'allrecipes.groceryserver.com'.
#     'Origin':'https://www.allrecipes.com'.
#     'Pragma':'no-cache'.
#     'Referer':urlToCheck,
#     'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0'.
# }