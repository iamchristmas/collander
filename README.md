# collander
Web-Scraping service for [ThePantryApp Project](https://www.github.com/iamchristmas/ThePantryApp.git) using Python

This project is undergoing active development and a static version of this page is viewable as an Azure App service [here](https://cupboard-dev.azurewebsites.net/)
